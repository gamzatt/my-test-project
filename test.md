# NetCracker Course Project

## Project mission:
Learn how to work with basic DevOps technologies by developing software for automated cloud infrastructure deployment.

## Description:
This project deploys cloud infrastructure on the YandexCloud platform.

### Scheme:
![image.png](./image.png)

## Prepare script
In file `main.tf` change YandexCloud parametrs: `token,` `cloud_id,` `folder_id` to yours:
```
provider "yandex" {
  token     = "<OAuth>"
  cloud_id  = "<cloud identifier>"
  folder_id = "<folder identifier>"
  zone      = "ru-central1-b"
}
```
## Start Deployment
1. Clone git repository to client machine
2. Run `init.sh` script with the parameters:

```bash
./init.sh <jenkins_username> <db_name> <postgresql_username> <mail> <mail_domain>
```
<hr>
* Jenkins jobs [repository](URL "https://gitlab.com/vikman_/jenkins_jobs")
